package com.youtube.sorcjc.billetero.ui.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.youtube.sorcjc.billetero.R
import com.youtube.sorcjc.billetero.model.TicketPlay
import kotlinx.android.synthetic.main.item_ticket_play.view.*


class TicketPlayAdapter (
    private var playsData: ArrayList<TicketPlay> = ArrayList(),
    private val allowRemoveItem: Boolean
)
    : RecyclerView.Adapter<TicketPlayAdapter.ViewHolder>() {

    fun addPlay(ticketPlay: TicketPlay) {
        playsData.add(ticketPlay)
        notifyItemInserted(playsData.size - 1)
    }

    fun addPoints(number: String, additionalPoints: Int) {
        for (i in 0..playsData.size) {
            if (playsData[i].number == number) {
                playsData[i].points += additionalPoints
                notifyItemChanged(i)
                break
            }
        }
    }

    fun findPlay(ticketPlay: TicketPlay): TicketPlay? {
        return playsData.find { play -> play.number == ticketPlay.number }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataSet(plays: ArrayList<TicketPlay>) {
        playsData = plays
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clear() {
        playsData.clear();
        notifyDataSetChanged();
    }

    fun getPlays(): ArrayList<TicketPlay> {
        return playsData;
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setTexts(ticketPlay: TicketPlay) = with (itemView) {
            tvNumber.text = ticketPlay.number
            tvPoints.text = ticketPlay.points.toString()
            tvType.text = ticketPlay.type

        }

        fun setDeleteButton(removeItem: Boolean, onClick: (View)->Unit) = with (itemView) {
            if (removeItem) {
                ibDelete.visibility = View.VISIBLE
                ibDelete.setOnClickListener(onClick)
            } else {
                ibDelete.visibility = View.GONE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_ticket_play, parent, false) as LinearLayout

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ticketPlay = playsData[position]

        holder.setTexts(ticketPlay)

        holder.setDeleteButton(allowRemoveItem) {
            playsData.remove(ticketPlay)
            notifyItemRemoved(position)
        }
    }

    override fun getItemCount() = playsData.size
}