package com.youtube.sorcjc.billetero.model

import com.google.gson.annotations.SerializedName

data class TicketPlay(
    val number: String,
    var points: Int = 0,
    val type: String,

    @SerializedName("ticket_id")
    val ticketId: Int
)